FROM cern/alma9-base

RUN dnf -y upgrade
RUN dnf -y install epel-release

RUN dnf install -y 'dnf-command(config-manager)'
RUN dnf config-manager --add-repo "https://storage-ci.web.cern.ch/storage-ci/eos/diopside/tag/testing/el-9/x86_64/"
RUN dnf config-manager --add-repo "https://storage-ci.web.cern.ch/storage-ci/eos/diopside-depend/el-9/x86_64/"

RUN dnf install -y --nogpgcheck xrootd-client eos-client
RUN dnf install -y python3-pip python3-devel python3-setuptools

COPY requirements.txt requirements.txt
RUN python3 -m pip install -r requirements.txt

COPY app /app