from prometheus_client import Gauge, start_http_server, Gauge
import time
import pandas as pd
import numpy as np
import os
from func import getLatestFile
import logging
import subprocess


MONITORING_PATH = os.getenv("MONITORING_PATH")


STATION_TEMP = Gauge('station_temp', 'Temperature of station', ['station', 'sensor'])
STATION_TEMP.labels("1", "Plane 1")
STATION_TEMP.labels("1", "Plane 2")
STATION_TEMP.labels("1", "Plane 3")
STATION_TEMP.labels("2", "Plane 1")
STATION_TEMP.labels("2", "Plane 2")
STATION_TEMP.labels("2", "Plane 3")
STATION_TEMP.labels("1", "Floating")
STATION_HUMIDITY = Gauge('station_humidity', 'Relative Humidity of station', ['station', 'sensor'])
STATION_HUMIDITY.labels("1", "SHT25")

CHILLER_TEMP = Gauge('chiller_temp', 'Temperature of chiller', ['station'])
CHILLER_TEMP.labels("1")

TENT_TEMP = Gauge('tent_temp', 'Temperature of tent')

STATION_TEMP.labels("1", "Frame")
STATION_TEMP.labels("1", "Upstream Modules")
STATION_TEMP.labels("1", "Downstream Modules")
STATION_TEMP.labels("1", "MUonE Sensor")

STATION_HUMIDITY.labels("1", "Frame")
STATION_HUMIDITY.labels("1", "Upstream Modules")
STATION_HUMIDITY.labels("1", "Downstream Modules")
STATION_HUMIDITY.labels("1", "MUonE Sensor")


if __name__ == '__main__':
    # Start up the server to expose the metrics.
    logging.info("Starting metrics server...")
    start_http_server(8000)
    # Generate some requests.
    while True:
        now = time.time()
        logging.debug("Getting latest file for Muone_s1_environment...")
        latest_file, latest_time = getLatestFile(MONITORING_PATH + "Muone_s1_environment/")
        if now - latest_time > 900:
            logging.warning(f"Latest file is older than 900 seconds ({now - latest_time}), setting metrics to NaN.")
            CHILLER_TEMP.labels("1").set(np.NaN)
            STATION_TEMP.labels("1", "Floating").set(np.NaN)
            STATION_TEMP.labels("1", "Plane 1").set(np.NaN)
            STATION_TEMP.labels("1", "Plane 2").set(np.NaN)
            STATION_TEMP.labels("1", "Plane 3").set(np.NaN)
            STATION_TEMP.labels("2", "Plane 1").set(np.NaN)
            STATION_TEMP.labels("2", "Plane 2").set(np.NaN)
            STATION_TEMP.labels("2", "Plane 3").set(np.NaN)
            STATION_HUMIDITY.labels("1", "SHT25").set(np.NaN)
            TENT_TEMP.set(np.NaN)
        else:
            subprocess.run(f"kinit -kt /secrets/dmonk.keytab dmonk@CERN.CH && xrdcp -N --force root://eospublic.cern.ch/{MONITORING_PATH}Muone_s1_environment/{latest_file} ./", shell=True, check=True)
            # df = pd.read_csv(latest_file, index_col=False, names=["timestamp", "chiller_T", "SHT25_RH", "SHT25_T", "DS18B20+_T_1", "DS18B20+_T_2", "DS18B20+_T_3", "DS18B20+_T_4", "DS18B20+_T_5", "DS18B20+_T_6", "dry_air_T"])
            df = pd.read_csv(latest_file, index_col=False, names=["timestamp", "chiller_T", "Humidity", "tent", "st1 plane1", "st1 plane2", "st1 plane3", "st2 plane1", "floating", "st2 plane2", "st2 plane3"])
            line = df.iloc[-1]
            CHILLER_TEMP.labels("1").set(line["chiller_T"])
            STATION_TEMP.labels("1", "Floating").set(line["floating"])
            STATION_TEMP.labels("1", "Plane 1").set(line["st1 plane1"])
            STATION_TEMP.labels("1", "Plane 2").set(line["st1 plane2"])
            STATION_TEMP.labels("1", "Plane 3").set(line["st1 plane3"])
            STATION_TEMP.labels("2", "Plane 1").set(line["st2 plane1"])
            STATION_TEMP.labels("2", "Plane 2").set(line["st2 plane2"])
            STATION_TEMP.labels("2", "Plane 3").set(line["st2 plane3"])
            STATION_HUMIDITY.labels("1", "SHT25").set(line["Humidity"])
            TENT_TEMP.set(line["tent"])
            os.remove(latest_file)

        # latest_file, latest_time = getLatestFile(MONITORING_PATH + "RhT_s1_environment/")
        # if now - latest_time > 900:
        #     logging.warning(f"Latest file is older than 900 seconds ({now - latest_time}), setting metrics to NaN.")
        #     STATION_TEMP.labels("1", "Frame").set(np.NaN)
        #     STATION_TEMP.labels("1", "Upstream Modules").set(np.NaN)
        #     STATION_TEMP.labels("1", "Downstream Modules").set(np.NaN)
        #     STATION_TEMP.labels("1", "MUonE Sensor").set(np.NaN)
        #     STATION_HUMIDITY.labels("1", "Frame").set(np.NaN)
        #     STATION_HUMIDITY.labels("1", "Upstream Modules").set(np.NaN)
        #     STATION_HUMIDITY.labels("1", "Downstream Modules").set(np.NaN)
        #     STATION_HUMIDITY.labels("1", "MUonE Sensor").set(np.NaN)
        # else:
        #     subprocess.run(f"kinit -kt /secrets/dmonk.keytab dmonk@CERN.CH && xrdcp -N --force root://eospublic.cern.ch/{MONITORING_PATH}RhT_s1_environment/{latest_file} ./", shell=True, check=True)
        #     lines = [i.strip().split(",") for i in open(latest_file)]
        #     a = [i[3:] for i in lines]
        #     df = pd.DataFrame(np.array(a[2:], dtype=float), columns=["HIH_0_T","HIH_0_H","HIH_1_T","HIH_1_H","HIH_2_T","HIH_2_H","HIH_3_T","HIH_3_H"])
        #     line = df.iloc[-1]
        #     STATION_TEMP.labels("1", "Frame").set(line.HIH_0_T)
        #     STATION_TEMP.labels("1", "Upstream Modules").set(line.HIH_1_T)
        #     STATION_TEMP.labels("1", "Downstream Modules").set(line.HIH_2_T)
        #     STATION_TEMP.labels("1", "MUonE Sensor").set(line.HIH_3_T)
        #     STATION_HUMIDITY.labels("1", "Frame").set(line.HIH_0_H)
        #     STATION_HUMIDITY.labels("1", "Upstream Modules").set(line.HIH_1_H)
        #     STATION_HUMIDITY.labels("1", "Downstream Modules").set(line.HIH_2_H)
        #     STATION_HUMIDITY.labels("1", "MUonE Sensor").set(line.HIH_3_H)
        #     os.remove(latest_file)

        time.sleep(5)