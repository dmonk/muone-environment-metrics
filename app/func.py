import subprocess
import os
import numpy as np
from datetime import datetime

def getMTime(filepath):
    p = subprocess.run(f"kinit -kt /secrets/dmonk.keytab dmonk@CERN.CH && eos fileinfo {filepath} -m", shell=True, check=True, capture_output=True)
    fileinfo = p.stdout.decode('latin1')
    return float([i for i in fileinfo.strip().split(' ') if "mtime" in i][0].split('=')[-1])

def getLatestFile(path):
    p = subprocess.run(f"kinit -kt /secrets/dmonk.keytab dmonk@CERN.CH && eos ls {path}", shell=True, check=True, capture_output=True)
    files = p.stdout.decode('latin1').strip().split('\n')
    dates = [datetime.strptime(i.split(".")[0].split("_")[-1], "%d%m%Y") for i in files]
    file = files[dates.index(max(dates))]
    return file, getMTime(f"{path}/{file}")

